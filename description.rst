========================
snowland-qyweixin-python
========================

.. image:: https://img.shields.io/pypi/v/snowland-qyweixin.svg
    :target: https://pypi.python.org/pypi/snowland-qyweixin

.. image:: https://img.shields.io/pypi/dm/snowland-qyweixin.svg
    :target: https://pypi.python.org/pypi/snowland-qyweixin

.. image:: https://img.shields.io/pypi/wheel/snowland-qyweixin.svg
    :target: https://pypi.python.org/pypi/snowland-qyweixin

.. image:: https://img.shields.io/pypi/status/snowland-qyweixin.svg
    :target: https://pypi.python.org/pypi/snowland-qyweixin